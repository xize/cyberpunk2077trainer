﻿using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CyberpunkTrainer
{
    public partial class SmtWindow : Form
    {

        private string searchpattern = "75 30 33 C9 B8 01 00 00 00 0F A2 8B C8 C1 F9 08";
        private string replacepattern = "EB 30 33 C9 B8 01 00 00 00 0F A2 8B C8 C1 F9 08";

        public SmtWindow()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("explorer.exe", "https://www.reddit.com/r/Amd/comments/kbp0np/cyberpunk_2077_seems_to_ignore_smt_and_mostly/");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult r = openFileDialog1.ShowDialog();
            if(r == DialogResult.OK)
            {
                if(openFileDialog1.CheckFileExists)
                {
                    flocation.Text = openFileDialog1.FileName;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            Cursor = Cursors.WaitCursor;
            bool b = PatternMatch(openFileDialog1.FileName);
            Cursor = Cursors.Default;

            if(b)
            {
                DialogResult r1 = MessageBox.Show("we found the signature and it is able to be patched\n\ncontinue progress Y/n", "Cyberpunk2077 SMT patcher", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if(r1 == DialogResult.No)
                {
                    return;
                }
            } else
            {
                MessageBox.Show("we failed to find the signature, maybe the file is already patched?", "Cyberpunk2077 SMT patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DialogResult r = MessageBox.Show("when you patch this file it cannot be undone, we will make a copy however with a _bak extension", "Cyberpunk2077 SMT patcher", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if(r == DialogResult.OK)
            {

                //first make copy.
                File.Copy(openFileDialog1.FileName, openFileDialog1.FileName + "_bak");
                //end copy

                Cursor = Cursors.WaitCursor;

                byte[] allbytes = File.ReadAllBytes(openFileDialog1.FileName);
                string exefile = BitConverter.ToString(allbytes).Replace("-", " ");

                Regex regex = new Regex(searchpattern);

                Match m1 = regex.Match(exefile);

                if(m1.Success)
                {
                    string newexe = StrReplace(exefile, m1.Index, m1.Length, replacepattern);
                    string[] newexelines = newexe.Split(' ');
                    byte[] bytes = new byte[newexelines.Length];
                    for(int i = 0; i < bytes.Length; i++)
                    {
                        bytes[i] = Convert.ToByte(newexelines[i], 16);
                    }


                    File.WriteAllBytes(openFileDialog1.FileName, bytes);
                    Cursor = Cursors.Default;
                    MessageBox.Show("the SMT patch has been successfully applied","Cyberpunk2077 SMT patcher", MessageBoxButtons.OK, MessageBoxIcon.Information);
                } else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("the SMT patch failed, usually you should not see this error!", "Cyberpunk2077 SMT patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private string StrReplace(string s, int index, int length, string replacement)
        {
            var builder = new StringBuilder();
            builder.Append(s.Substring(0, index));
            builder.Append(replacement);
            builder.Append(s.Substring(index + length));
            return builder.ToString();
        }

        private bool PatternMatch(string file)
        {
            byte[] allbytes = File.ReadAllBytes(openFileDialog1.FileName);
            string exefile = BitConverter.ToString(allbytes).Replace("-", " ");

            if(exefile.Contains(searchpattern))
            {
                return true;
            }
            return false;
        }
    }
}
