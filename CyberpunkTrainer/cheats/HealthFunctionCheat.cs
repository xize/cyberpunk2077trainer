﻿using CyberpunkTrainer.cheats.api;
using CyberpunkTrainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberpunkTrainer.cheats
{
    class HealthFunctionCheat : CheatFramework
    {

        private Mem m;
        private CustomCheckbox cs;

        private Cheat cheat;

        public HealthFunctionCheat(Mem m, CustomCheckbox cs)
        {
            this.m = m;
            this.cs = cs;
            this.cheat = new Cheat(this.m, "health function", cs);

        }

        private UIntPtr GodmodeAddress
        {
            get;set;
        }

        private UIntPtr InstakillAddress
        {
            get;set;
        }

        public bool Godmode
        {
            get {
                int i = this.m.ReadInt((this.m.Is64Bit ? GodmodeAddress.ToUInt64().ToString("X") : GodmodeAddress.ToUInt32().ToString("X")));
                if(i == 1)
                {
                    return true;
                }
                return false;
            }
            set {

                Logger.Log("setting godmode flag at " + (this.m.Is64Bit ? GodmodeAddress.ToUInt64().ToString("X") : GodmodeAddress.ToUInt32().ToString("X")) + " to "+(value ? "1" : "0"), Logger.LogType.INFO);
                this.m.WriteMemory((this.m.Is64Bit ? GodmodeAddress.ToUInt64().ToString("X") : GodmodeAddress.ToUInt32().ToString("X")), "int", (value ? 1 : 0)+"");
            }
        }

        public bool Instakill
        {
            get
            {
                int i = this.m.ReadInt((this.m.Is64Bit ? InstakillAddress.ToUInt64().ToString("X") : InstakillAddress.ToUInt32().ToString("X")));
                if (i == 1)
                {
                    return true;
                }
                return false;
            }
            set
            {
                Logger.Log("setting instakill flag at " + (this.m.Is64Bit ? InstakillAddress.ToUInt64().ToString("X") : InstakillAddress.ToUInt32().ToString("X")) + " to "+(value ? "1" : "0"), Logger.LogType.INFO);
                this.m.WriteMemory((this.m.Is64Bit ? InstakillAddress.ToUInt64().ToString("X") : InstakillAddress.ToUInt32().ToString("X")), "int", (value ? 1 : 0) + "");
            }
        }

        public bool Enable()
        {

            Logger.Log("initializing health function...", Logger.LogType.INFO);

            this.cheat.AobScan("F3 0F 11 82 90 01 00 00").AddCaveBytes(new byte[]
            {

                0x66,0x9C,0x81,0xBA,0x7C,
                0x01,0x00,0x00,0x2B,0x00,
                0x00,0x00,0x0F,0x84,0x28,
                0x00,0x00,0x00,0x81,0xBA,
                0x7C,0x01,0x00,0x00,0x00,
                0x00,0x00,0x00,0x0F,0x85,
                0x64,0x00,0x00,0x00,0x83,
                0xBD,0xF8,0x00,0x00,0x00,
                0x50,0x0F,0x8E,0x29,0x00,
                0x00,0x00,0x0F,0x85,0x51,
                0x00,0x00,0x00,0xE9,0x48,
                0xF8,0x93,0x01,0x83,0x3D,
                0x54,0x00,0x00,0x00,0x01,
                0x0F,0x85,0x3F,0x00,0x00,
                0x00,0x66,0x9D,0xC7,0x82,
                0x90,0x01,0x00,0x00,0x00,
                0x00,0xC8,0x42,0xE9,0x2A,
                0xF8,0x93,0x01,0x83,0x3D,
                0x3A,0x00,0x00,0x00,0x01,
                0x0F,0x85,0x21,0x00,0x00,
                0x00,0x81,0xBA,0x90,0x01,
                0x00,0x00,0x00,0x00,0x80,
                0x3F,0x0F,0x8E,0x11,0x00,
                0x00,0x00,0x66,0x9D,0xC7,
                0x82,0x90,0x01,0x00,0x00,
                0x00,0x00,0x80,0x3F,0xE9,
                0xFC,0xF7,0x93,0x01,0x66,
                0x9D,0xF3,0x0F,0x11,0x82,
                0x90,0x01,0x00,0x00

            }).RestoreJumpInstructionBytes(new byte[]
            {
                0xF3,0x0F,0x11,0x82,0x90,
                0x01,0x00,0x00
            });

            bool b = this.cheat.Execute();

            if(b)
            {
                this.GodmodeAddress = UIntPtr.Add(this.cheat.AllocatedAddress, 0x95);
                this.InstakillAddress = UIntPtr.Add(this.GodmodeAddress, 0x4);
            }

            if(b)
            {
                Logger.Log("code cave created on address: 0x"+(this.m.Is64Bit ? this.cheat.AllocatedAddress.ToUInt64().ToString("X") : this.cheat.AllocatedAddress.ToUInt32().ToString("X")), Logger.LogType.INFO);
                Logger.Log("health flag from codecave is: 0x" + (this.m.Is64Bit ? this.GodmodeAddress.ToUInt64().ToString("X") : this.GodmodeAddress.ToUInt32().ToString("X")), Logger.LogType.INFO);
                Logger.Log("instakill flag from codecave is: 0x" + (this.m.Is64Bit ? this.InstakillAddress.ToUInt64().ToString("X") : this.InstakillAddress.ToUInt32().ToString("X")), Logger.LogType.INFO);
            } else
            {
                Logger.Log("code cave creation failed, either not enough memory, outdated trainer, or anti virus protection", Logger.LogType.SEVERE);
            }

            return b;
        }

        public void Disable()
        {
            Logger.Log("disabling health function, restoring orginal address", Logger.LogType.INFO);
            this.cheat.Restore();
        }

        
    }
}
