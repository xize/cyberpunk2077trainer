﻿using CyberpunkTrainer.cheats.api;
using CyberpunkTrainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberpunkTrainer.cheats
{
    public class NoPoliceCheat
    {

        private Mem m;
        private CustomCheckbox cs;
        private Cheat cheat;

        public NoPoliceCheat(Mem m, CustomCheckbox cs)
        {
            this.m = m;
            this.cs = cs;
            this.cheat = new Cheat(this.m, "no police cheat", this.cs);
        }

        public bool Enable()
        {
            Logger.Log("initializing no police cheat", Logger.LogType.INFO);

            this.cheat.AobScan("58 03 F3 0F 11 03 48 85 F6").
            InjectionAOBOffset(2).
            AddCaveBytes(new byte[] {

                0x66,0x9C,0x48,0x3D,0x00,
                0x00,0x80,0x3F,0x0F,0x85,
                0x0E,0x00,0x00,0x00,0x66,
                0x9D,0xF3,0x0F,0x11,0x1B,
                0x48,0x85,0xF6,0xE9,0xDC,
                0x9B,0x24,0x00,0x66,0x9D,
                0xF3,0x0F,0x11,0x03,0x48,
                0x85,0xF6

            }).RestoreJumpInstructionBytes(new byte[] {

                0xF3,0x0F,0x11,0x03,0x48,
                0x85,0xF6

            });

            bool b = this.cheat.Execute();

            if (b)
            {
                Logger.Log("code cave created on address: 0x" + (this.m.Is64Bit ? this.cheat.AllocatedAddress.ToUInt64().ToString("X") : this.cheat.AllocatedAddress.ToUInt32().ToString("X")), Logger.LogType.INFO);
            }
            else
            {
                Logger.Log("code cave creation failed, either not enough memory, outdated trainer, or anti virus protection", Logger.LogType.SEVERE);
            }

            return b;
        }

        public void Disable()
        {
            Logger.Log("disabling no police cheat, restoring orginal address", Logger.LogType.INFO);
            this.cheat.Restore();
        }

    }
}
