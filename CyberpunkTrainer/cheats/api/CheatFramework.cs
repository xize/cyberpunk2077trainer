﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberpunkTrainer.cheats.api
{
    interface CheatFramework
    {

        bool Enable();

        void Disable();

    }

}
