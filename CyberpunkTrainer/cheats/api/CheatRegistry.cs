﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CyberpunkTrainer.cheats.api
{
    public class CheatRegistry
    {

        private static List<Cheat> cheats = new List<Cheat>();

        public static void Add(Cheat c)
        {
            cheats.Add(c);
        }

        public static void DisableAll(Window w, bool closing = false)
        {
            foreach (Cheat cheat in cheats)
            {
                if (cheat.HasCheckbox)
                {
                    if (cheat.CustomCheckBox() == null)
                    {
                        MessageBox.Show("the checkbox is null on " + cheat.CheatName);
                        break;
                    }

                    if (closing)
                    {
                        cheat.Restore();
                    }
                    else
                    {
                        if (cheat.CustomCheckBox().Checked)
                        {
                            cheat.CustomCheckBox().PerformClick();
                        }
                    }
                }
            }
            return;
        }

    }

}
