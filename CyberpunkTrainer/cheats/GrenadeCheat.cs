﻿using CyberpunkTrainer.cheats.api;
using CyberpunkTrainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberpunkTrainer.cheats
{
    public class GrenadeCheat : CheatFramework
    {

        private Mem m;
        private CustomCheckbox cs;
        private Cheat cheat;

        public GrenadeCheat(Mem m, CustomCheckbox cs)
        {
            this.m = m;
            this.cs = cs;
            this.cheat = new Cheat(this.m, "infinitive grenades", this.cs);
        }


        public bool Enable()
        {
            Logger.Log("initializing infinitive grenades", Logger.LogType.INFO);
            this.cheat.AobScan("89 97 E8 00 00 00 48 83 C4 20").AddCaveBytes(new byte[]
            {
                0xC7,0x87,0xE8,0x00,0x00,
                0x00,0x63,0x00,0x00,0x00

            }).RestoreJumpInstructionBytes(new byte[]
            {
                0x89,0x97,0xE8,0x00,0x00,
                0x00
            });

            bool b = this.cheat.Execute();
            
            if(b)
            {
                Logger.Log("code cave created on address: 0x" + (this.m.Is64Bit ? this.cheat.AllocatedAddress.ToUInt64().ToString("X") : this.cheat.AllocatedAddress.ToUInt32().ToString("X")), Logger.LogType.INFO);
            } else
            {
                Logger.Log("code cave creation failed, either not enough memory, outdated trainer, or anti virus protection", Logger.LogType.SEVERE);
            }

            return b;
        }

        public void Disable()
        {
            Logger.Log("disabling infinitive grenades, restoring orginal address", Logger.LogType.INFO);
            this.cheat.Restore();
        }
    }
}
