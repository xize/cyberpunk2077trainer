﻿using CyberpunkTrainer.cheats.api;
using CyberpunkTrainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberpunkTrainer.cheats
{
    public class ManaCheat : CheatFramework
    {

        private Mem m;
        private CustomCheckbox cs;
        private Cheat cheat;

        public ManaCheat(Mem m, CustomCheckbox cs)
        {
            this.m = m;
            this.cs = cs;
            this.cheat = new Cheat(this.m, "infinitive mana", this.cs);
        }

        public bool Enable()
        {
            Logger.Log("initializing infinitive mana", Logger.LogType.INFO);
            this.cheat.AobScan("F3 0F 11 82 90 01 00 00").AddCaveBytes(new byte[] {
                0x66,0x9C,0x49,0x83,0xFC,
                0x00,0x0F,0x85,0x0F,0x00,
                0x00,0x00,0xF3,0x0F,0x11,
                0x9A,0x90,0x01,0x00,0x00,
                0x66,0x9D,0xE9,0x67,0xF8,
                0x93,0x01,0x66,0x9D,0xF3,
                0x0F,0x11,0x82,0x90,0x01,
                0x00,0x00
            }).RestoreJumpInstructionBytes(new byte[] {
                0xF3,0x0F,0x11,0x82,0x90,
                0x01,0x00,0x00
            });

            bool b = this.cheat.Execute();

            if (b)
            {
                Logger.Log("code cave created on address: 0x" + (this.m.Is64Bit ? this.cheat.AllocatedAddress.ToUInt64().ToString("X") : this.cheat.AllocatedAddress.ToUInt32().ToString("X")), Logger.LogType.INFO);
            }
            else
            {
                Logger.Log("code cave creation failed, either not enough memory, outdated trainer, or anti virus protection", Logger.LogType.SEVERE);
            }

            return b;
        }

        public void Disable()
        {
            Logger.Log("disabling infinitive mana, restoring orginal address", Logger.LogType.INFO);
            this.cheat.Restore();
        }
    }
}
