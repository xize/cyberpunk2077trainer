﻿using CyberpunkTrainer.cheats.api;
using CyberpunkTrainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberpunkTrainer.cheats
{
    class AmmoCheat : CheatFramework
    {

        private Mem m;
        private CustomCheckbox cs;
        private Cheat ammocheat;
        private Cheat clipcheat;

        public AmmoCheat(Mem m, CustomCheckbox cs)
        {
            this.m = m;
            this.cs = cs;
            this.ammocheat = new Cheat(this.m, "inf ammo (no clip)", this.cs);
            this.clipcheat = new Cheat(this.m, "inf ammo (clip)", this.cs);
        }

        private string AmmoAddress
        {
            get;set;
        }

        public bool Enable()
        {

            Logger.Log("initializing infinitive ammo and infinitive ammo clip", Logger.LogType.INFO);

            this.ammocheat.AobScan("44 89 A6 40 03 00 00").AddCaveBytes(new byte[]
            {
                0x49,0x83,0xFD,0x00,0x0F,
                0x85,0x06,0x00,0x00,0x00,
                0x90,0xE9,0xEE,0x5B,0xA6,
                0x01,0x44,0x89,0xA6,0x40,
                0x03,0x00,0x00
            }).RestoreJumpInstructionBytes(new byte[]
            {
                0x44,0x89,0xA6,0x40,0x03,
                0x00,0x00
            });

            this.clipcheat.AobScan("89 57 78 48 83 C4 20").AddCaveBytes(new byte[]
            {
                0x66,0x9C,0x48,0x83,0xFE,
                0x00,0x0F,0x85,0x28,0x00,
                0x00,0x00,0x83,0x7D,0x47,
                0x00,0x0F,0x84,0x0B,0x00,
                0x00,0x00,0x0F,0x85,0x18,
                0x00,0x00,0x00,0xE9,0xE6,
                0x84,0x7B,0x01,0xBA,0x63,
                0x00,0x00,0x00,0x89,0x57,
                0x78,0x66,0x9D,0x48,0x83,
                0xC4,0x20,0xE9,0xD3,0x84,
                0x7B,0x01,0x89,0x57,0x78,
                0x66,0x9D,0x48,0x83,0xC4,
                0x20
            }).RestoreJumpInstructionBytes(new byte[]
            {
                0x89,0x57,0x78,0x48,0x83,
                0xC4,0x20
            });

            bool b1 = this.ammocheat.Execute();
            bool b2 = this.clipcheat.Execute();

            if(b1 && b2)
            {
                Logger.Log("for ammo: ", Logger.LogType.INFO); ;
                Logger.Log("code cave created on address: 0x" + (this.m.Is64Bit ? this.ammocheat.AllocatedAddress.ToUInt64().ToString("X") : this.ammocheat.AllocatedAddress.ToUInt32().ToString("X")), Logger.LogType.INFO);
                Logger.ForceLine();
                Logger.Log("for ammo clip: ", Logger.LogType.INFO); ;
                Logger.Log("code cave created on address: 0x" + (this.m.Is64Bit ? this.clipcheat.AllocatedAddress.ToUInt64().ToString("X") : this.clipcheat.AllocatedAddress.ToUInt32().ToString("X")), Logger.LogType.INFO);
            } else
            {
                Logger.Log("code cave creation failed, either not enough memory, outdated trainer, or anti virus protection", Logger.LogType.SEVERE);
            }

            return b1 && b2;
        }

        public void Disable()
        {
            Logger.Log("disabling inf ammo, restoring orginal addresses", Logger.LogType.INFO);
            this.ammocheat.Restore();
            this.clipcheat.Restore();
        }
    }
}
