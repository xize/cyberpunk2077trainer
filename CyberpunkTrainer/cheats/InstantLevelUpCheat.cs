﻿using CyberpunkTrainer.cheats.api;
using CyberpunkTrainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CyberpunkTrainer.cheats
{
    public class InstantLevelUpCheat : CheatFramework
    {

        private Mem m;
        private CustomCheckbox cs;
        private Cheat cheat;

        public InstantLevelUpCheat(Mem m, CustomCheckbox cs)
        {
            this.m = m;
            this.cs = cs;
            this.cheat = new Cheat(this.m, "instant level up", this.cs);
        }

        private UIntPtr XPAddy
        {
            get;set;
        }

        private UIntPtr StreetAddy
        {
            get;set;
        }

        public bool SetXPFlag(bool flag)
        {
            if(XPAddy == UIntPtr.Zero)
            {
                return false;
            }

            string address = (this.m.Is64Bit ? XPAddy.ToUInt64().ToString("X") : XPAddy.ToUInt32().ToString("X"));

            bool b = this.m.WriteMemory(address, "int", (flag ? 1 : 0) + "");

            return b;
        }

        public bool SetStreetCredFlag(bool flag)
        {
            if (StreetAddy == UIntPtr.Zero)
            {
                return false;
            }

            string address = (this.m.Is64Bit ? StreetAddy.ToUInt64().ToString("X") : StreetAddy.ToUInt32().ToString("X"));

            bool b = this.m.WriteMemory(address, "int", (flag ? 1 : 0) + "");
            return b;
        }

        public bool Enable()
        {
            this.cheat.AobScan("89 02 C3 CC CC CC CC CC CC CC CC CC CC CC CC CC CC CC 41 8B 00 89 02 C3").
            InjectionAOBOffset(18).
            AddCaveBytes(new byte[] {
                0x66,0x9C,0x49,0x83,0xFA,
                0x1A,0x0F,0x85,0x69,0x00,
                0x00,0x00,0x48,0x83,0xFB,
                0x00,0x0F,0x85,0x5F,0x00,
                0x00,0x00,0x81,0x7D,0x18,
                0xE8,0x03,0x00,0x00,0x0F,
                0x84,0x12,0x00,0x00,0x00,
                0x81,0x7D,0x10,0x0D,0x00,
                0x00,0x00,0x0F,0x84,0x25,
                0x00,0x00,0x00,0xE9,0x40,
                0x00,0x00,0x00,0x83,0x3D,
                0x45,0x00,0x00,0x00,0x01,
                0x0F,0x85,0x33,0x00,0x00,
                0x00,0x41,0xC7,0x00,0x3F,
                0x42,0x0F,0x00,0x41,0x8B,
                0x00,0x89,0x02,0x66,0x9D,
                0xE9,0x40,0x93,0x1D,0x00,
                0x83,0x3D,0x29,0x00,0x00,
                0x00,0x01,0x0F,0x85,0x13,
                0x00,0x00,0x00,0x41,0xC7,
                0x00,0x3F,0x42,0x0F,0x00,
                0x41,0x8B,0x00,0x89,0x02,
                0x66,0x9D,0xE9,0x20,0x93,
                0x1D,0x00,0x66,0x9D,0x41,
                0x8B,0x00,0x89,0x02
            }).RestoreJumpInstructionBytes(new byte[] {
                0x41,0x8B,0x00,0x89,0x02,
                0xC3
            });

            bool b = this.cheat.Execute();
            if(b)
            {
                UIntPtr xp = UIntPtr.Add(this.cheat.AllocatedAddress, 0x81);
                UIntPtr street = UIntPtr.Add(xp, 0x4);

                this.XPAddy = xp;
                this.StreetAddy = street;
            }

            return b;
        }

        public void Disable()
        {
            this.cheat.Restore();
            this.XPAddy = UIntPtr.Zero;
            this.StreetAddy = UIntPtr.Zero;
        }
    }
}
