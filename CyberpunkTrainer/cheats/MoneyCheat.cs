﻿using CyberpunkTrainer.cheats.api;
using CyberpunkTrainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberpunkTrainer.cheats
{
    public class MoneyCheat : CheatFramework
    {

        private Mem m;
        private CustomCheckbox cs;
        private Cheat cheat;

        public MoneyCheat(Mem m, CustomCheckbox cs)
        {
            this.m = m;
            this.cs = cs;
            this.cheat = new Cheat(this.m, "infinitive money", this.cs);
        }


        public bool Enable()
        {
            this.cheat.AobScan("89 57 78 48 83 C4 ?? ?? ?? CC").AddCaveBytes(new byte[]
            {
                0x66,0x9C,0x48,0x83,0xFE,
                0x00,0x0F,0x85,0x20,0x00,
                0x00,0x00,0x83,0xBD,0xC7,
                0x00,0x00,0x00,0x01,0x0F,
                0x84,0x13,0x00,0x00,0x00,
                0xBA,0x3F,0x42,0x0F,0x00,
                0x89,0x57,0x78,0x48,0x83,
                0xC4,0x20,0x66,0x9D,0xE9,
                0xDB,0x84,0x7B,0x01,0x66,
                0x9D,0x89,0x57,0x78,0x48,
                0x83,0xC4,0x20
            }).RestoreJumpInstructionBytes(new byte[]
            {
                0x89,0x57,0x78,0x48,0x83,
                0xC4,0x20
            });

            return this.cheat.Execute();
        }

        public void Disable()
        {
            this.cheat.Restore();
        }
    }
}
