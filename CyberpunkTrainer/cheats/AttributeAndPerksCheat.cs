﻿using CyberpunkTrainer.cheats.api;
using CyberpunkTrainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberpunkTrainer.cheats
{
    public class AttributeAndPerksCheat : CheatFramework
    {

        private Mem m;
        private CustomCheckbox cs;
        private Cheat cheat;

        public AttributeAndPerksCheat(Mem m, CustomCheckbox cs)
        {
            this.m = m;
            this.cs = cs;
            this.cheat = new Cheat(this.m, "infinitive attributes and perks", this.cs);
        }

        public bool Enable()
        {
            this.cheat.AobScan("48 8D 2D 74 51 A1 03 45 33 F6 C6 42 60 01 44 89 74 24 48 49 8B F0 4C 89 72 30 4C 8D 44 24 48 4C 89 72 38 45 33 C9 0F B6 08 48 8B FA 48 FF C0 48 89 02 8B C1 48 8B 4A 40 FF 54 C5 00 48 8B 47 30 48 8D 5C 24 48 48 85 C0 44 88 77 60 44 89 74 24 50 4C 8D 44 24 50 48 0F 45 D8 4C 89 77 30 48 8B 07 45 33 C9 4C 89 77 38 48 8B D7 0F B6 08 48 FF C0 48 89 07 8B C1 48 8B 4F 40 FF 54 C5 00 48 FF 07 8B 03 2B 44 24 50 89 03").
                InjectionAOBOffset(135).                                                                                                                                                                                                                                                                                                                                                                                                           //^ inject here                                          
                AddCaveBytes(new byte[] {
                    0x66,0x9C,0x49,0x83,0xFA,
                    0x1A,0x0F,0x85,0x1B,0x00,
                    0x00,0x00,0x0F,0x84,0x05,
                    0x00,0x00,0x00,0xE9,0x6A,
                    0xB4,0x24,0x00,0x66,0x9D,
                    0xC7,0x03,0x63,0x00,0x00,
                    0x00,0x48,0x85,0xF6,0xE9,
                    0x5A,0xB4,0x24,0x00,0x66,
                    0x9D,0x89,0x03,0x48,0x85,
                    0xF6
                }).RestoreJumpInstructionBytes(new byte[]
                {
                    0x89,0x03,0x48,0x85,0xF6
                });
            return this.cheat.Execute();
        }

        public void Disable()
        {
            this.cheat.Restore();
        }
    }
}
