﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberpunkTrainer
{
    public class Logger
    {

        public static void LogLogo()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("d888888b d88888b  .d8b.  .88b  d88.      d8888b. d88888b db    db d888888b db    db d88888b ");
            Console.WriteLine("`~~88~~' 88'     d8' `8b 88'YbdP`88      88  `8D 88'     88    88   `88'   88    88 88'     ");
            Console.WriteLine("   88    88ooooo 88ooo88 88  88  88      88oobY' 88ooooo Y8    8P    88    Y8    8P 88ooooo ");
            Console.WriteLine("   88    88~~~~~ 88~~~88 88  88  88      88`8b   88~~~~~ `8b  d8'    88    `8b  d8' 88~~~~~ ");
            Console.WriteLine("   88    88.     88   88 88  88  88      88 `88. 88.      `8bd8'    .88.    `8bd8'  88.     ");
            Console.WriteLine("   YP    Y88888P YP   YP YP  YP  YP      88   YD Y88888P    YP    Y888888P    YP    Y88888P ");
            Console.WriteLine("--------------------------------------------------------------------------------------------");
            Console.WriteLine("Production: beta");
            Console.WriteLine();
            Console.ResetColor();
        }

        public static void Log(string message, LogType type)
        {
            if(type == LogType.INFO)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("["+DateTime.Now.ToString("d/M/y h:m:s")+"]");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("[info]");
                Console.ResetColor();
                Console.Write(message + "\n");
            } else
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("[" + DateTime.Now.ToString("d/M/y h:m:s") + "]");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("[warning]");
                Console.ResetColor();
                Console.Write(message + "\n");
            }
        }

        public static void ForceLine()
        {
            Console.WriteLine();
        }

        public enum LogType
        {
            INFO,
            SEVERE
        }

    }
}
