﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberpunkTrainer.errors
{
    partial class ErrorWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorWindow));
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.Exceptionname = new System.Windows.Forms.Label();
            this.errorpanel = new System.Windows.Forms.Panel();
            this.copybtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(0, 37);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(372, 125);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // Exceptionname
            // 
            this.Exceptionname.AutoSize = true;
            this.Exceptionname.Location = new System.Drawing.Point(40, 2);
            this.Exceptionname.Name = "Exceptionname";
            this.Exceptionname.Size = new System.Drawing.Size(174, 26);
            this.Exceptionname.TabIndex = 1;
            this.Exceptionname.Text = "A Exception has been occuried.\r\nPlease check the stacktrace below";
            // 
            // errorpanel
            // 
            this.errorpanel.Location = new System.Drawing.Point(4, 0);
            this.errorpanel.Name = "errorpanel";
            this.errorpanel.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.errorpanel.Size = new System.Drawing.Size(33, 31);
            this.errorpanel.TabIndex = 2;
            // 
            // copybtn
            // 
            this.copybtn.Location = new System.Drawing.Point(285, 168);
            this.copybtn.Name = "copybtn";
            this.copybtn.Size = new System.Drawing.Size(75, 23);
            this.copybtn.TabIndex = 3;
            this.copybtn.Text = "copy";
            this.copybtn.UseVisualStyleBackColor = true;
            this.copybtn.Click += new System.EventHandler(this.Copybtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(204, 168);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ErrorWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 191);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.copybtn);
            this.Controls.Add(this.errorpanel);
            this.Controls.Add(this.Exceptionname);
            this.Controls.Add(this.richTextBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ErrorWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ErrorWindow";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ErrorWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label Exceptionname;
        private System.Windows.Forms.Panel errorpanel;
        private System.Windows.Forms.Button copybtn;
        private System.Windows.Forms.Button button1;
    }

}
