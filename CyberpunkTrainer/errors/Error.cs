﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberpunkTrainer.errors
{
    public class Error
    {
        public static void ErrorNotHooked()
        {
            ErrorWindow errw = new ErrorWindow();
            errw.Title = "Error";
            errw.TextArea = "The trainer is not hooked";
            errw.ShowDialog();
        }

        public static void ErrorAOBNotFound(string aob, string cheat)
        {
            ErrorWindow errw = new ErrorWindow();
            errw.Title = "Error";
            errw.TextArea = String.Format("the following aob was not found when enabling this cheat.\n\nCheat:{0}\nAOB:{1}\n\nmake a ticket to https://gitlab.com/xize/cyberpunk2077trainer", cheat, aob);
            errw.ShowDialog();
        }

        public static void ErrorAddressNotFound(string cheat)
        {
            ErrorWindow errw = new ErrorWindow();
            errw.Title = "Error";
            errw.TextArea = String.Format("The code cave creation failed because the address was null for cheat {0}", cheat);
            errw.ShowDialog();
        }

        public static void ErrorEmptyCodeCave(string cheat)
        {
            ErrorWindow errw = new ErrorWindow();
            errw.Title = "Error";
            errw.TextArea = String.Format("The code cave creation failed because the bytes in the code cave where empty for cheat {0}", cheat);
            errw.ShowDialog();
        }

        public static void ErrorRestoreJMPBytesEmpty(string cheat)
        {
            ErrorWindow errw = new ErrorWindow();
            errw.Title = "Error";
            errw.TextArea = String.Format("The code cave creation failed because the bytes to restore the orginal instruction where empty for cheat {0}", cheat);
            errw.ShowDialog();
        }

        public static void ErrorCodeCaveCreationFailed(string cheat)
        {
            ErrorWindow errw = new ErrorWindow();
            errw.Title = "Error";
            errw.TextArea = String.Format("The code cave creation failed, the return pointer after code cave creation returned null for cheat {0}", cheat);
            errw.ShowDialog();
        }

        public static void ErrorUnableToAllocateSpace(string cheat, string aob, string address)
        {

            ErrorWindow errw = new ErrorWindow();
            errw.Title = "Error";
            errw.TextArea = String.Format("unable to find space to allocate memory for cheat {0} within 35 seconds!\nthis could be caused by a game protection or by AV as the AOB (signature) address was found.\n--------\n\nFound signature address: 0x{2}\nsignature:{1}", cheat, aob, address);
            errw.ShowDialog();
        }

        public static void ErrorAOBOffset()
        {
            ErrorWindow errw = new ErrorWindow();
            errw.Title = "Error";
            errw.TextArea = "Your AOBOffset should not be 0 or lower than 0!";
            errw.ShowDialog();
        }

    }

}
