﻿using CyberpunkTrainer.cheats;
using CyberpunkTrainer.cheats.api;
using CyberpunkTrainer.errors;
using Memory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CyberpunkTrainer
{
    public partial class Window : Form
    {

        private Mem m;

        public event EventHandler<HookEventArgs> HookChanged;

        #region cheatfields
        private HealthFunctionCheat healthfunctioncheat;
        private GrenadeCheat grenadecheat;
        private AmmoCheat ammocheat;
        private AttributeAndPerksCheat attributeperkcheat;
        private ManaCheat manacheat;
        private NoPoliceCheat nopolicecheat;
        private MoneyCheat moneycheat;
        private BackpackCheat backpackcheat;
        private InstantLevelUpCheat levelupcheat;
        #endregion

        public Window()
        {
            InitializeComponent();
        }

        private void Window_Load(object sender, EventArgs e)
        {
            Process[] procs = Process.GetProcessesByName("hmpalert");
            if(procs.Length >= 1)
            {
                Logger.Log("detected hitman.alert this program inteferes with our code cave creation awaiting user response.", Logger.LogType.SEVERE);
                DialogResult r = MessageBox.Show("it seems you are running hitman.alert, we know security is important for you.\nbut this program is unable to run in this kind of environment.\n\nEven if you disable process hollowing and Codecaves it still makes the internal process memory unreadable for us by the invasive sandboxing and whitelisting dated from 13/12/2020 will not work.\n\nDo you want to proceed further Y/n?", "Incompatible software found", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if(r == DialogResult.No)
                {
                    Application.Exit();
                }
            }

            this.Hide();
            this.HookChanged += (s, o) =>
            {
                if(!o.Status)
                {
                    CheatRegistry.DisableAll(this, false);
                }
            };

            this.moveAbleWindow1.ControlTarget = new Control[] { this };
            this.version.Text = String.Format(this.version.Text, Application.ProductVersion);
            this.worker.RunWorkerAsync();
            this.SetHotKeys();
            this.fadeControl1.Start();
        }

        #region DllImports
        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vlc);
        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);
        #endregion

        #region hotkeys
        public void SetHotKeys()
        {
            RegisterHotKey(this.Handle, 0, 0, Keys.NumPad0.GetHashCode());
            RegisterHotKey(this.Handle, 1, 0, Keys.NumPad1.GetHashCode());
            RegisterHotKey(this.Handle, 2, 0, Keys.NumPad2.GetHashCode());
            RegisterHotKey(this.Handle, 3, 0, Keys.NumPad3.GetHashCode());
            RegisterHotKey(this.Handle, 4, 0, Keys.NumPad4.GetHashCode());
            RegisterHotKey(this.Handle, 5, 0, Keys.NumPad5.GetHashCode());
            RegisterHotKey(this.Handle, 6, 0, Keys.NumPad6.GetHashCode());
            RegisterHotKey(this.Handle, 7, 0, Keys.NumPad7.GetHashCode());
            RegisterHotKey(this.Handle, 8, 0, Keys.NumPad8.GetHashCode());
            RegisterHotKey(this.Handle, 9, 0, Keys.NumPad8.GetHashCode());
        }

        protected override void WndProc(ref Message msg)
        {
            if (msg.Msg == 0x0312) //our hot button recognition and function triggering
            {
                int id = msg.WParam.ToInt32();

                if (id == 0)
                {
                    if (!this.healthfunccheckbox.Checked)
                    {
                        MessageBox.Show("You need to enable the Health function first manually", "Cyberpunk2077 trainer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    this.godmodecheckbox.PerformClick();
                } else if (id == 1)
                {
                    if (!this.healthfunccheckbox.Checked)
                    {
                        MessageBox.Show("You need to enable the Health function first manually", "Cyberpunk2077 trainer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    this.instakillcheckbox.PerformClick();
                } else if (id == 2)
                {
                    this.grenadecheckbox.PerformClick();
                } else if (id == 3)
                {
                    this.infammocheckbox.PerformClick();
                } else if (id == 4)
                {
                    this.attributeperkcheckbox.PerformClick();   
                } else if(id == 5)
                {
                    this.manacheckbox.PerformClick();
                } else if(id == 6)
                {
                    this.nopolicecheckbox.PerformClick();
                } else if(id == 7)
                {
                    this.moneycheckbox.PerformClick();
                } else if(id == 8)
                {
                    this.backpackcheckbox.PerformClick();
                } else if(id == 9)
                {
                    this.backpackdmgcheckbox.PerformClick();
                }
            }
            base.WndProc(ref msg);
        }

        #endregion


        private bool hk = false;
        public bool HookStatus
        {
            get { return hk; }
            set
            {
                if(value)
                {
                    this.hookstat.Text = "true";
                    this.hookstat.ForeColor = Color.LightGreen;
                    this.modulebaseaddr.ForeColor = Color.LightGreen;

                    IntPtr a = IntPtr.Zero;
                    this.m.modules.TryGetValue("Cyberpunk2077.exe", out a);
                    this.modulebaseaddr.Text = "0x" + (this.m.Is64Bit ? a.ToInt64().ToString("X") : a.ToInt32().ToString("X"));
                    
                    Console.Beep(500, 100);
                    Console.Beep(200, 100);
                    Console.Beep(500, 100);
                }
                else
                {
                    this.hookstat.Text = "false";
                    this.hookstat.ForeColor = Color.Red;
                    this.modulebaseaddr.ForeColor = Color.Red;
                    this.modulebaseaddr.Text = "0x0";
                }
                this.hk = value;
                this.Update();
            }
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void panel2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if(this.m == null)
            {
                this.m = new Mem();
            }

            while(true)
            {
                
                if(this.m.theProc == null || this.m.theProc.HasExited)
                {
                    bool b = this.m.OpenProcess(this.m.GetProcIdFromName("Cyberpunk2077"));
                    if(b)
                    {
                        this.worker.ReportProgress(1);
                    } else
                    {
                        this.worker.ReportProgress(0);
                    }
                }

                //for performance reasons
                Thread.Sleep(3 * 60 * 60);

            }
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if(e.ProgressPercentage == 0)
            {
                Logger.Log("the process could not be found or is unable to hook!", Logger.LogType.SEVERE);
                this.HookStatus = false;
                EventHandler<HookEventArgs> handler = HookChanged;
                if(handler != null)
                {
                    HookEventArgs ev = new HookEventArgs(false);
                    handler.Invoke(this, ev);
                }
            } else
            {
                Logger.ForceLine();
                Logger.Log("the process has been hooked!", Logger.LogType.INFO);
                Logger.Log("process version: "+ this.m.theProc.MainModule.FileVersionInfo.FileVersion, Logger.LogType.INFO);
                Logger.Log("process: "+this.m.theProc.ProcessName+".exe", Logger.LogType.INFO);
                Logger.Log("PID: " + this.m.theProc.Id, Logger.LogType.INFO);
                Logger.Log("the process is a "+(this.m.Is64Bit ? "64 bit process" : "32 bit process"), Logger.LogType.INFO);
                Logger.ForceLine();
                this.HookStatus = true;
                EventHandler<HookEventArgs> handler = HookChanged;
                if (handler != null)
                {
                    HookEventArgs ev = new HookEventArgs(true);
                    handler.Invoke(this, ev);
                }
            }
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void modulebaseaddr_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(this.modulebaseaddr.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.HookStatus)
            {
                MessageBox.Show("cannot run the patcher when the game is running, please close!", "Cyberpunk 2077 Trainer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            SmtWindow smt = new SmtWindow();
            smt.ShowDialog();
        }

        private void onclosecleanup(object sender, FormClosingEventArgs e)
        {
            if (this.HookStatus)
            {
                CheatRegistry.DisableAll(this, false);
            }
        }

        #region cheatevents
        private void HealthFunctionEnable(object sender, controls.CheckboxChangeArgs e)
        {
            if (e.NewState)
            {

                if(this.healthfunctioncheat == null)
                {
                    this.healthfunctioncheat = new HealthFunctionCheat(this.m, this.healthfunccheckbox);
                }

                bool b = this.healthfunctioncheat.Enable();

                if(b)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        this.godmodecheckbox.Enabled = true;
                        this.instakillcheckbox.Enabled = true;
                    }));
                } else
                {
                    e.Cancel = true;
                }

            }
            else
            {
                this.healthfunctioncheat.Disable();

                this.Invoke(new MethodInvoker(delegate
                {
                    this.godmodecheckbox.Checked = false;
                    this.instakillcheckbox.Checked = false;
                    this.godmodecheckbox.Enabled = false;
                    this.instakillcheckbox.Enabled = false;
                }));
            }
        }

        private void godmodecheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if(e.NewState)
            {
                this.healthfunctioncheat.Godmode = true;
            } else
            {
                this.healthfunctioncheat.Godmode = false;
            }
        }

        private void instakillcheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if (e.NewState)
            {
                this.healthfunctioncheat.Instakill = true;
            }
            else
            {
                this.healthfunctioncheat.Instakill = false;
            }
        }

        private void infammocheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if(e.NewState)
            {
                if(this.ammocheat == null)
                {
                    this.ammocheat = new AmmoCheat(this.m, this.infammocheckbox);
                }

                bool b = this.ammocheat.Enable();
               if(!b)
                {
                    e.Cancel = true;
                }

            } else
            {
                this.ammocheat.Disable();
            }
        }

        private void grenadecheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if(e.NewState)
            {
                if (this.grenadecheat == null)
                {
                    this.grenadecheat = new GrenadeCheat(this.m, this.grenadecheckbox);
                }

                bool b = this.grenadecheat.Enable();
                if(!b)
                {
                    e.Cancel = true;
                }
            } else
            {
                this.grenadecheat.Disable();
            }
        }

        private void manacheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if(e.NewState)
            {
                if(this.manacheat == null)
                {
                    this.manacheat = new ManaCheat(this.m, this.manacheckbox);
                }

                bool b = this.manacheat.Enable();
                if(!b)
                {
                    e.Cancel = true;
                }
            } else
            {
                this.manacheat.Disable();
            }
        }

        private void nopolicecheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if(e.NewState)
            {
                if(this.nopolicecheat == null)
                {
                    this.nopolicecheat = new NoPoliceCheat(this.m, this.nopolicecheckbox);
                }

                bool b = this.nopolicecheat.Enable();
                if(!b)
                {
                    e.Cancel = true;
                }
            } else
            {
                this.nopolicecheat.Disable();
            }
        }

        private void attributeperkcheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if(e.NewState)
            {
                if(this.attributeperkcheat == null)
                {
                    this.attributeperkcheat = new AttributeAndPerksCheat(this.m, this.attributeperkcheckbox);
                }

                bool b = this.attributeperkcheat.Enable();
                if(!b)
                {
                    e.Cancel = true;
                    this.Invoke(new MethodInvoker(() =>
                    {
                        this.levelxpcheckbox.Checked = false;
                        this.levelxpcheckbox.Enabled = false;
                        this.streetcredcheckbox.Checked = false;
                        this.streetcredcheckbox.Enabled = false;
                    }));
                }
            } else
            {
                this.attributeperkcheat.Disable();
            }
        }

        private void moneycheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if(e.NewState)
            {
                if(this.moneycheat == null)
                {
                    this.moneycheat = new MoneyCheat(this.m, this.moneycheckbox);
                }

                bool b = this.moneycheat.Enable();
                if(!b)
                {
                    e.Cancel = true;
                }
            } else
            {
                this.moneycheat.Disable();
            }
        }

        private void backpackcheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if(e.NewState)
            {
                if(this.backpackcheat == null)
                {
                    this.backpackcheat = new BackpackCheat(this.m, this.backpackcheckbox);
                }

                bool b = this.backpackcheat.Enable();
                if (b)
                {
                    this.Invoke(new MethodInvoker(() =>
                   {
                       this.backpackdmgcheckbox.Enabled = true;
                   }));     
                } else
                {
                    e.Cancel = true;
                }
            } else
            {
                this.backpackcheat.Disable();
                this.Invoke(new MethodInvoker(() =>
                {
                    this.backpackdmgcheckbox.Checked = false;
                    this.backpackdmgcheckbox.Enabled = false;
                }));
            }
        }

        private void backpackdmgcheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if(!this.backpackcheckbox.Checked)
            {
                MessageBox.Show("cannot enable child cheat when parent cheat is not enabled, please enable backpack cheat first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                backpackdmgcheckbox.Enabled = false;
                return;
            }

            if(e.NewState)
            {
                if(this.backpackcheat == null)
                {
                    this.backpackcheckbox.PerformClick();
                }

                this.backpackcheat.SetMaxStatFlag(true);
            } else
            {
                if (this.backpackcheat == null)
                {
                    this.backpackcheckbox.PerformClick();
                }

                this.backpackcheat.SetMaxStatFlag(false);
            }
        }

        private void inventoryxpcheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if(e.NewState)
            {
                if(this.levelupcheat == null)
                {
                    this.levelupcheat = new InstantLevelUpCheat(this.m, this.inventoryxpcheckbox);
                }

                bool b = this.levelupcheat.Enable();
                
                if(b)
                {
                    this.Invoke(new MethodInvoker(() =>
                    {
                        this.levelxpcheckbox.Enabled = true;
                        this.streetcredcheckbox.Enabled = true;
                    }));

                } else
                {
                    e.Cancel = true;
                }
            } else
            {
                this.levelupcheat.Disable();
                this.Invoke(new MethodInvoker(() =>
                {
                    this.levelxpcheckbox.Checked = false;
                    this.streetcredcheckbox.Checked = false;
                    this.levelxpcheckbox.Enabled = false;
                    this.streetcredcheckbox.Enabled = false;
                }));
            }
        }

        private void levelxpcheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if(!this.inventoryxpcheckbox.Checked)
            {
                MessageBox.Show("cannot enable child cheat when parent cheat is not enabled, please enable \"inventory xps\" cheat first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                backpackdmgcheckbox.Enabled = false;
                return;
            }

            if(e.NewState)
            {
                this.levelupcheat.SetXPFlag(true);
            } else
            {
                this.levelupcheat.SetXPFlag(false);
            }
        }

        private void streetcredcheckbox_CheckboxChange(object sender, controls.CheckboxChangeArgs e)
        {
            if (!this.inventoryxpcheckbox.Checked)
            {
                MessageBox.Show("cannot enable child cheat when parent cheat is not enabled, please enable \"inventory xps\" cheat first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                backpackdmgcheckbox.Enabled = false;
                return;
            }

            if (e.NewState)
            {
                this.levelupcheat.SetStreetCredFlag(true);
            }
            else
            {
                this.levelupcheat.SetStreetCredFlag(false);
            }
        }
        #endregion
    }
}
