﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CyberpunkTrainer.controls
{
    public partial class RoundedObject : Component
    {

        [DllImport("Gdi32.dll", EntryPoint = "DeleteObject")]
        public static extern bool DeleteObject(IntPtr hObject);

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        private int radius = 30;
        private Control c;
        private bool eventinst = false;

        public RoundedObject()
        {

        }

        public Control TargetControl
        {
            get { return c; }
            set
            {
                this.c = value;
                this.MakeRoundCorners();
                this.c.VisibleChanged += new EventHandler(onChangevis);
                this.c.SizeChanged += new EventHandler(onChangevis);
                this.c.Invalidated += new InvalidateEventHandler(onChangevis_invalidate);
            }
        }

        public int Radius
        {
            get { return this.radius; }
            set
            {
                this.radius = value;
                this.MakeRoundCorners();
            }
        }

        private int bordersize = 0;
        public int Border
        {
            get
            {
                return bordersize;
            }
            set
            {
                this.bordersize = value;
                if (this.c != null)
                {
                    this.c.Update();
                }
            }
        }

        private Color bcolor = Color.Transparent;
        public Color BorderColor
        {
            get
            {
                return this.bcolor;
            }
            set
            {
                this.bcolor = value;
                if (this.c != null)
                {
                    this.c.Update();
                }
            }
        }

        private void MakeRoundCorners()
        {
            if (c == null)
            {
                return;
            }

            IntPtr handle;
            this.c.Region = System.Drawing.Region.FromHrgn(handle = CreateRoundRectRgn(0, 0, c.Width, c.Height, radius, radius));

            if (!this.eventinst)
            {
                this.c.Paint += new PaintEventHandler((s, e) =>
                {
                    Pen myPen = new Pen(BorderColor);
                    myPen.Width = Border;

                    // Set the LineJoin property
                    myPen.LineJoin = System.Drawing.Drawing2D.LineJoin.Round;


                    e.Graphics.DrawRectangle(myPen, c.ClientRectangle);
                });
                this.eventinst = !this.eventinst;
            }


            if (handle != null)
            {
                DeleteObject(handle);
            }

        }

        private void onChangevis(object sender, EventArgs e)
        {
            this.MakeRoundCorners();
        }

        private void onChangevis_invalidate(object sender, InvalidateEventArgs e)
        {

            this.MakeRoundCorners();
        }

        public void Update()
        {
            this.MakeRoundCorners();
        }
    }

}
