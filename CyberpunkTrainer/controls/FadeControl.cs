﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CyberpunkTrainer.controls
{
    public class FadeControl : Component
    {
        private double progress = 0.0;
        private Random rand = new Random();
        private BackgroundWorker worker = new BackgroundWorker();

        /**
         * <summary>fires when the fade effect has been finished!</summary>
         */
        public event EventHandler FadeFinish;

        public FadeControl()
        {
            InitializeComponent();
            this.worker.WorkerSupportsCancellation = true;
            this.worker.DoWork += new DoWorkEventHandler(Work);
            this.worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Completed);
        }

        private void InitializeComponent()
        {

        }

        /**
         * <summary>sets or returns the fading form</summary>
         */
        public Form Target
        {
            get; set;
        }

        /**
         * <summary>starts the fading effect</summary>
         * <exception cref="Exception">throws exception when no form is specified</exception>
         */
        public void Start()
        {
            if (Target == null)
            {
                throw new System.Exception("no target form specified for fade control");
            }
            Target.Opacity = progress;
            worker.RunWorkerAsync();

        }
        private void Work(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                bool cclose = false;

                //synchronize :)
                this.Target.Invoke((MethodInvoker)delegate ()
                {
                    {
                        if (this.Target.Opacity >= 1.0)
                        {

                            if (progress == 0)
                            {
                                double b = this.progress;
                                this.progress = this.progress + rand.Next(0, 13) + this.rand.NextDouble();
                                if (this.progress >= 100)
                                {
                                    this.progress = 100;
                                }
                            }
                            else if (progress >= 100)
                            {
                                cclose = !cclose;
                            }
                            else
                            {

                                double b = this.progress;
                                this.progress = this.progress + rand.Next(0, 13) + this.rand.NextDouble();
                                if (this.progress >= 100)
                                {
                                    this.progress = 100;
                                }
                            }

                        }
                        else
                        {
                            this.Target.Opacity += 0.1;
                        }
                    }
                });

                if (cclose)
                {
                    break;
                }

                Thread.Sleep(100);
            }
        }

        private void Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            EventHandler handler = FadeFinish;
            if (handler != null)
            {
                handler?.Invoke(this.Target, EventArgs.Empty);
            }
        }
    }
}
