﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CyberpunkTrainer.controls
{
    public partial class CustomCheckbox : UserControl
    {
        private bool isbutton = false;
        private bool chkd = false;

        public event EventHandler ButtonClick;
        public event EventHandler<CheckboxChangeArgs> CheckboxChange;


        public CustomCheckbox()
        {
            InitializeComponent();
        }

        public bool Checked
        {
            get
            {
                return this.chkd;
            }
            set
            {
                Point p = this.ball.Location;
                if(value)
                {
                    p.X = 10;
                    this.ball.BackColor = Color.FromArgb(180, 255, 255, 0);
                } else
                {
                    p.X = 2;
                    this.ball.BackColor = Color.FromArgb(70, 255, 255, 0);

                }
                this.ball.Location = p;
                this.chkd = value;
                this.Update();
            }
        }

        public bool IsButton
        {
            get
            {
                return this.isbutton;
            }
            set
            {
                if(value)
                {
                    Point p = this.ball.Location;
                    p.X= 6;
                    this.ball.Location = p;
                } else
                {
                    Point p = this.ball.Location;
                    p.X = 2;
                    this.ball.Location = p;
                }
                this.isbutton = value;
            }
        }

        private void CustomCheckbox_EnabledChanged(object sender, EventArgs e)
        {
            //this.ball.BackColor = Color.FromArgb(180, 255, 255, 0);
            //this.ball.BackColor = Color.FromArgb(30, 255, 255, 0);
            if(this.IsButton)
            {
                if(this.Enabled)
                {
                    Point p = this.ball.Location;
                    p.X = 6;
                    this.ball.Location = p;
                    this.ball.BackColor = Color.FromArgb(180, 255, 255, 0);
                } else
                {
                    Point p = this.ball.Location;
                    p.X = 6;
                    this.ball.Location = p;
                    this.ball.BackColor = Color.FromArgb(10, 255, 255, 0);
                }
            } else
            {
                if(this.Enabled)
                {
                    Point p = this.ball.Location;
                    p.X = 2;
                    this.ball.Location = p;
                    this.ball.BackColor = Color.FromArgb(70, 255, 255, 0);
                } else
                {
                    Point p = this.ball.Location;
                    p.X = 6;
                    this.ball.Location = p;
                    this.ball.BackColor = Color.FromArgb(10, 255, 255, 0);

                }
            }
            this.Update();
        }

        private async void ball_Click(object sender, EventArgs e)
        {
            if(this.IsButton)
            {
                EventHandler handler = ButtonClick;
                if(handler != null)
                {
                    this.Enabled = false;
                    this.ParentForm.Cursor = Cursors.WaitCursor;
                    this.Cursor = Cursors.WaitCursor;
                    this.ball.Cursor = Cursors.WaitCursor;

                    await Task.Run(() => handler.Invoke(this, EventArgs.Empty));

                    this.Enabled = true;
                    this.ParentForm.Cursor = Cursors.Default;
                    this.Cursor = Cursors.Hand;
                    this.ball.Cursor = Cursors.Hand;
                    if (!Muted)
                    {
                        Console.Beep(500, 100);
                        Console.Beep(900, 100);
                        Console.Beep(100, 100);
                    }

                }
            } else
            {
                EventHandler<CheckboxChangeArgs> handler = CheckboxChange;
                if(handler != null)
                {
                    this.Enabled = false;
                    this.ParentForm.Cursor = Cursors.WaitCursor;
                    this.Cursor = Cursors.WaitCursor;
                    this.ball.Cursor = Cursors.WaitCursor;

                    //state as of now.
                    bool currentstate = this.Checked;

                    CheckboxChangeArgs args = new CheckboxChangeArgs(false, currentstate, !currentstate);
                    await Task.Run(() => handler.Invoke(this, args));

                    if(args.Cancel)
                    {
                            this.Enabled = true;
                            this.ParentForm.Cursor = Cursors.Default;
                            this.Cursor = Cursors.Hand;
                            this.ball.Cursor = Cursors.Hand;
                            if (!Muted)
                            {
                                Console.Beep(200, 100);
                                Console.Beep(300, 100);
                                Console.Beep(100, 100);
                            }
                            this.Update();

                        return;
                    } else
                    {
                            this.Enabled = true;
                            this.Checked = !currentstate;
                            this.ParentForm.Cursor = Cursors.Default;
                            this.Cursor = Cursors.Hand;
                            this.ball.Cursor = Cursors.Hand;

                        if (!Muted)
                        {
                            if (this.Checked)
                            {
                                Console.Beep(500, 100);
                                Console.Beep(900, 100);
                                Console.Beep(100, 100);
                            }
                            else
                            {
                                Console.Beep(200, 100);
                                Console.Beep(300, 100);
                                Console.Beep(100, 100);
                            }
                        }

                            this.Update();
                    }
                }
            }
        }

        private bool Muted
        {
            get
            {
                if(this.ParentForm is Window)
                {
                    Window w = (Window)this.ParentForm;
                    if(w.mutecheckbox.Checked)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public void PerformClick()
        {
            ball_Click(this, EventArgs.Empty);
        }
    }
}
