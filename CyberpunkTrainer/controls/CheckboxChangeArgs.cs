﻿namespace CyberpunkTrainer.controls
{
    public class CheckboxChangeArgs
    {

        private bool isCancelled = false;

        private bool previousstate;
        private bool newstate;

        public CheckboxChangeArgs(bool cancel, bool prevstate, bool newstate)
        {
            this.isCancelled = cancel;
            this.previousstate = prevstate;
            this.newstate = newstate;
        }


        public bool NewState
        {
            get{
                return this.newstate;
            }
        }

        public bool OldState
        {
            get
            {
                return this.previousstate;
            }
        }

        public bool Cancel
        {
            get { return this.isCancelled; }
            set { this.isCancelled = value; }
        }

    }
}