﻿
namespace CyberpunkTrainer.controls
{
    partial class CustomCheckbox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.formround = new CyberpunkTrainer.controls.RoundedObject();
            this.ball = new System.Windows.Forms.Panel();
            this.leverround = new CyberpunkTrainer.controls.RoundedObject();
            this.SuspendLayout();
            // 
            // formround
            // 
            this.formround.Border = 0;
            this.formround.BorderColor = System.Drawing.Color.Transparent;
            this.formround.Radius = 10;
            this.formround.TargetControl = this;
            // 
            // ball
            // 
            this.ball.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            this.ball.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ball.Location = new System.Drawing.Point(2, 2);
            this.ball.Name = "ball";
            this.ball.Size = new System.Drawing.Size(14, 10);
            this.ball.TabIndex = 0;
            this.ball.Click += new System.EventHandler(this.ball_Click);
            // 
            // leverround
            // 
            this.leverround.Border = 0;
            this.leverround.BorderColor = System.Drawing.Color.Transparent;
            this.leverround.Radius = 5;
            this.leverround.TargetControl = this.ball;
            // 
            // CustomCheckbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Controls.Add(this.ball);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Name = "CustomCheckbox";
            this.Size = new System.Drawing.Size(30, 15);
            this.EnabledChanged += new System.EventHandler(this.CustomCheckbox_EnabledChanged);
            this.Click += new System.EventHandler(this.ball_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private RoundedObject formround;
        private System.Windows.Forms.Panel ball;
        private RoundedObject leverround;
    }
}
