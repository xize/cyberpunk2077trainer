﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CyberpunkTrainer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine();
            Console.Title = "Cyberpunk trainer console (Do not close)";
            Logger.LogLogo();
            Logger.Log("starting trainer for cyberpunk target version is " + ConvertVersion(), Logger.LogType.INFO);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Window());

            //Console.ReadKey();
        }

        public static string ConvertVersion()
        {
            string v = Application.ProductVersion.Substring(Application.ProductVersion.LastIndexOf('.') + 1);

            string newstring = "";

            char[] chars = v.ToCharArray();
            foreach(char c in chars)
            {
                newstring += c + ".";
            }

            newstring = newstring.Substring(0, newstring.LastIndexOf('.'));
            return newstring;
        }
    }
}
