﻿
namespace CyberpunkTrainer
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.healthpanel = new System.Windows.Forms.Panel();
            this.instakillcheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.godmodecheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.healthfunccheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.version = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.modulebaseaddr = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.mutecheckbox = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.hookstat = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.worker = new System.ComponentModel.BackgroundWorker();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.backpackdmgcheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.label13 = new System.Windows.Forms.Label();
            this.backpackcheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.label16 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.streetcredcheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.label18 = new System.Windows.Forms.Label();
            this.levelxpcheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.label11 = new System.Windows.Forms.Label();
            this.inventoryxpcheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.label17 = new System.Windows.Forms.Label();
            this.moneycheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.attributeperkcheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.nopolicecheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.manacheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.infammocheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.grenadecheckbox = new CyberpunkTrainer.controls.CustomCheckbox();
            this.roundedObject1 = new CyberpunkTrainer.controls.RoundedObject();
            this.moveAbleWindow1 = new CyberpunkTrainer.controls.MoveAbleWindow();
            this.roundedObject2 = new CyberpunkTrainer.controls.RoundedObject();
            this.fadeControl1 = new CyberpunkTrainer.controls.FadeControl();
            this.roundedObject3 = new CyberpunkTrainer.controls.RoundedObject();
            this.roundedObject4 = new CyberpunkTrainer.controls.RoundedObject();
            this.healthpanel.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::CyberpunkTrainer.Properties.Resources.close;
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Location = new System.Drawing.Point(564, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(16, 19);
            this.panel1.TabIndex = 0;
            this.panel1.Click += new System.EventHandler(this.panel1_Click);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::CyberpunkTrainer.Properties.Resources.minimize;
            this.panel2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel2.Location = new System.Drawing.Point(549, 14);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(18, 19);
            this.panel2.TabIndex = 1;
            this.panel2.Click += new System.EventHandler(this.panel2_Click);
            // 
            // healthpanel
            // 
            this.healthpanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.healthpanel.Controls.Add(this.instakillcheckbox);
            this.healthpanel.Controls.Add(this.godmodecheckbox);
            this.healthpanel.Controls.Add(this.label3);
            this.healthpanel.Controls.Add(this.label2);
            this.healthpanel.Controls.Add(this.healthfunccheckbox);
            this.healthpanel.Controls.Add(this.label1);
            this.healthpanel.Location = new System.Drawing.Point(282, 67);
            this.healthpanel.Name = "healthpanel";
            this.healthpanel.Size = new System.Drawing.Size(140, 63);
            this.healthpanel.TabIndex = 20;
            // 
            // instakillcheckbox
            // 
            this.instakillcheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.instakillcheckbox.Checked = false;
            this.instakillcheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.instakillcheckbox.Enabled = false;
            this.instakillcheckbox.IsButton = false;
            this.instakillcheckbox.Location = new System.Drawing.Point(103, 45);
            this.instakillcheckbox.Name = "instakillcheckbox";
            this.instakillcheckbox.Size = new System.Drawing.Size(30, 15);
            this.instakillcheckbox.TabIndex = 13;
            this.instakillcheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.instakillcheckbox_CheckboxChange);
            // 
            // godmodecheckbox
            // 
            this.godmodecheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.godmodecheckbox.Checked = false;
            this.godmodecheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.godmodecheckbox.Enabled = false;
            this.godmodecheckbox.IsButton = false;
            this.godmodecheckbox.Location = new System.Drawing.Point(103, 28);
            this.godmodecheckbox.Name = "godmodecheckbox";
            this.godmodecheckbox.Size = new System.Drawing.Size(30, 15);
            this.godmodecheckbox.TabIndex = 12;
            this.godmodecheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.godmodecheckbox_CheckboxChange);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label3.Location = new System.Drawing.Point(28, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "num 1 instakill:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label2.Location = new System.Drawing.Point(28, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "num 0 godmode:";
            // 
            // healthfunccheckbox
            // 
            this.healthfunccheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.healthfunccheckbox.Checked = false;
            this.healthfunccheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.healthfunccheckbox.IsButton = false;
            this.healthfunccheckbox.Location = new System.Drawing.Point(80, 7);
            this.healthfunccheckbox.Name = "healthfunccheckbox";
            this.healthfunccheckbox.Size = new System.Drawing.Size(30, 15);
            this.healthfunccheckbox.TabIndex = 9;
            this.healthfunccheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.HealthFunctionEnable);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label1.Location = new System.Drawing.Point(5, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Health Function:";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = global::CyberpunkTrainer.Properties.Resources.d7grgua_bb56b55a_4978_4b88_b616_4d0c4f57d63d;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Location = new System.Drawing.Point(422, 260);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(180, 226);
            this.panel3.TabIndex = 23;
            // 
            // version
            // 
            this.version.AutoSize = true;
            this.version.BackColor = System.Drawing.Color.Transparent;
            this.version.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.version.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.version.Location = new System.Drawing.Point(284, 451);
            this.version.Name = "version";
            this.version.Size = new System.Drawing.Size(116, 13);
            this.version.TabIndex = 26;
            this.version.Text = "Version: {0} - By Team Revive";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel4.Controls.Add(this.modulebaseaddr);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.mutecheckbox);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.hookstat);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Location = new System.Drawing.Point(22, 74);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(237, 92);
            this.panel4.TabIndex = 27;
            // 
            // modulebaseaddr
            // 
            this.modulebaseaddr.AutoSize = true;
            this.modulebaseaddr.BackColor = System.Drawing.Color.Transparent;
            this.modulebaseaddr.Cursor = System.Windows.Forms.Cursors.Hand;
            this.modulebaseaddr.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modulebaseaddr.ForeColor = System.Drawing.Color.Red;
            this.modulebaseaddr.Location = new System.Drawing.Point(82, 39);
            this.modulebaseaddr.Name = "modulebaseaddr";
            this.modulebaseaddr.Size = new System.Drawing.Size(21, 13);
            this.modulebaseaddr.TabIndex = 20;
            this.modulebaseaddr.Text = "0x0";
            this.modulebaseaddr.Click += new System.EventHandler(this.modulebaseaddr_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(3, 38);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 13);
            this.label20.TabIndex = 19;
            this.label20.Text = "Module base start:";
            // 
            // mutecheckbox
            // 
            this.mutecheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mutecheckbox.AutoSize = true;
            this.mutecheckbox.BackColor = System.Drawing.Color.Transparent;
            this.mutecheckbox.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mutecheckbox.ForeColor = System.Drawing.Color.White;
            this.mutecheckbox.Location = new System.Drawing.Point(4, 65);
            this.mutecheckbox.Name = "mutecheckbox";
            this.mutecheckbox.Size = new System.Drawing.Size(71, 17);
            this.mutecheckbox.TabIndex = 18;
            this.mutecheckbox.Text = "Mute sound";
            this.mutecheckbox.UseVisualStyleBackColor = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.LightGreen;
            this.label15.Location = new System.Drawing.Point(40, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Cyberpunk2077.exe";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(3, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Target:";
            // 
            // hookstat
            // 
            this.hookstat.AutoSize = true;
            this.hookstat.BackColor = System.Drawing.Color.Transparent;
            this.hookstat.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hookstat.ForeColor = System.Drawing.Color.Red;
            this.hookstat.Location = new System.Drawing.Point(42, 22);
            this.hookstat.Name = "hookstat";
            this.hookstat.Size = new System.Drawing.Size(26, 13);
            this.hookstat.TabIndex = 15;
            this.hookstat.Text = "false";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(3, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Hooked:";
            // 
            // worker
            // 
            this.worker.WorkerReportsProgress = true;
            this.worker.WorkerSupportsCancellation = true;
            this.worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.worker_DoWork);
            this.worker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.worker_ProgressChanged);
            this.worker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.worker_RunWorkerCompleted);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(38, 446);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(174, 23);
            this.button1.TabIndex = 28;
            this.button1.Text = "Open SMT patcher for AMD performance.";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label4.Location = new System.Drawing.Point(285, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "num 2: infintive grenades";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label5.Location = new System.Drawing.Point(285, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "num 3: Infinitive ammo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label7.Location = new System.Drawing.Point(285, 187);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "num 5: inf mana";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label9.Location = new System.Drawing.Point(285, 204);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.TabIndex = 37;
            this.label9.Text = "num 6: no police";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label6.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(394, 187);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 45;
            this.label6.Text = "[unstable]";
            this.toolTip1.SetToolTip(this.label6, "instakill and some other cheats cause issues.");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label8.Location = new System.Drawing.Point(285, 171);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(147, 13);
            this.label8.TabIndex = 46;
            this.label8.Text = "num 4: infinitive perks and attributes";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label10.Location = new System.Drawing.Point(285, 219);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(124, 13);
            this.label10.TabIndex = 48;
            this.label10.Text = "num 7: give 999999 euro creds";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel5.Controls.Add(this.backpackdmgcheckbox);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.backpackcheckbox);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Location = new System.Drawing.Point(282, 235);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(222, 41);
            this.panel5.TabIndex = 21;
            // 
            // backpackdmgcheckbox
            // 
            this.backpackdmgcheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.backpackdmgcheckbox.Checked = false;
            this.backpackdmgcheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backpackdmgcheckbox.Enabled = false;
            this.backpackdmgcheckbox.IsButton = false;
            this.backpackdmgcheckbox.Location = new System.Drawing.Point(181, 22);
            this.backpackdmgcheckbox.Name = "backpackdmgcheckbox";
            this.backpackdmgcheckbox.Size = new System.Drawing.Size(30, 15);
            this.backpackdmgcheckbox.TabIndex = 12;
            this.backpackdmgcheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.backpackdmgcheckbox_CheckboxChange);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label13.Location = new System.Drawing.Point(19, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(160, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "num 9: max everything (dmg|armor|skills)";
            // 
            // backpackcheckbox
            // 
            this.backpackcheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.backpackcheckbox.Checked = false;
            this.backpackcheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backpackcheckbox.IsButton = false;
            this.backpackcheckbox.Location = new System.Drawing.Point(119, 6);
            this.backpackcheckbox.Name = "backpackcheckbox";
            this.backpackcheckbox.Size = new System.Drawing.Size(30, 15);
            this.backpackcheckbox.TabIndex = 9;
            this.backpackcheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.backpackcheckbox_CheckboxChange);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label16.Location = new System.Drawing.Point(5, 6);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(110, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "num 8: inf backpack weight";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel6.Controls.Add(this.streetcredcheckbox);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.levelxpcheckbox);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.inventoryxpcheckbox);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Location = new System.Drawing.Point(282, 279);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(133, 58);
            this.panel6.TabIndex = 22;
            // 
            // streetcredcheckbox
            // 
            this.streetcredcheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.streetcredcheckbox.Checked = false;
            this.streetcredcheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.streetcredcheckbox.Enabled = false;
            this.streetcredcheckbox.IsButton = false;
            this.streetcredcheckbox.Location = new System.Drawing.Point(94, 37);
            this.streetcredcheckbox.Name = "streetcredcheckbox";
            this.streetcredcheckbox.Size = new System.Drawing.Size(30, 15);
            this.streetcredcheckbox.TabIndex = 14;
            this.streetcredcheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.streetcredcheckbox_CheckboxChange);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label18.Location = new System.Drawing.Point(19, 37);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "F3: Street Cred XP";
            // 
            // levelxpcheckbox
            // 
            this.levelxpcheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.levelxpcheckbox.Checked = false;
            this.levelxpcheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.levelxpcheckbox.Enabled = false;
            this.levelxpcheckbox.IsButton = false;
            this.levelxpcheckbox.Location = new System.Drawing.Point(74, 22);
            this.levelxpcheckbox.Name = "levelxpcheckbox";
            this.levelxpcheckbox.Size = new System.Drawing.Size(30, 15);
            this.levelxpcheckbox.TabIndex = 12;
            this.levelxpcheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.levelxpcheckbox_CheckboxChange);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label11.Location = new System.Drawing.Point(19, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "F2: Level XP";
            // 
            // inventoryxpcheckbox
            // 
            this.inventoryxpcheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.inventoryxpcheckbox.Checked = false;
            this.inventoryxpcheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.inventoryxpcheckbox.IsButton = false;
            this.inventoryxpcheckbox.Location = new System.Drawing.Point(76, 6);
            this.inventoryxpcheckbox.Name = "inventoryxpcheckbox";
            this.inventoryxpcheckbox.Size = new System.Drawing.Size(30, 15);
            this.inventoryxpcheckbox.TabIndex = 9;
            this.inventoryxpcheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.inventoryxpcheckbox_CheckboxChange);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Impact", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(111)))));
            this.label17.Location = new System.Drawing.Point(5, 6);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "F1: Inventory XPs";
            // 
            // moneycheckbox
            // 
            this.moneycheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.moneycheckbox.Checked = false;
            this.moneycheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.moneycheckbox.IsButton = false;
            this.moneycheckbox.Location = new System.Drawing.Point(415, 219);
            this.moneycheckbox.Name = "moneycheckbox";
            this.moneycheckbox.Size = new System.Drawing.Size(30, 15);
            this.moneycheckbox.TabIndex = 49;
            this.moneycheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.moneycheckbox_CheckboxChange);
            // 
            // attributeperkcheckbox
            // 
            this.attributeperkcheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.attributeperkcheckbox.Checked = false;
            this.attributeperkcheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.attributeperkcheckbox.IsButton = false;
            this.attributeperkcheckbox.Location = new System.Drawing.Point(438, 171);
            this.attributeperkcheckbox.Name = "attributeperkcheckbox";
            this.attributeperkcheckbox.Size = new System.Drawing.Size(30, 15);
            this.attributeperkcheckbox.TabIndex = 47;
            this.attributeperkcheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.attributeperkcheckbox_CheckboxChange);
            // 
            // nopolicecheckbox
            // 
            this.nopolicecheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.nopolicecheckbox.Checked = false;
            this.nopolicecheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nopolicecheckbox.IsButton = false;
            this.nopolicecheckbox.Location = new System.Drawing.Point(359, 204);
            this.nopolicecheckbox.Name = "nopolicecheckbox";
            this.nopolicecheckbox.Size = new System.Drawing.Size(30, 15);
            this.nopolicecheckbox.TabIndex = 38;
            this.nopolicecheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.nopolicecheckbox_CheckboxChange);
            // 
            // manacheckbox
            // 
            this.manacheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.manacheckbox.Checked = false;
            this.manacheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.manacheckbox.IsButton = false;
            this.manacheckbox.Location = new System.Drawing.Point(358, 187);
            this.manacheckbox.Name = "manacheckbox";
            this.manacheckbox.Size = new System.Drawing.Size(30, 15);
            this.manacheckbox.TabIndex = 34;
            this.manacheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.manacheckbox_CheckboxChange);
            // 
            // infammocheckbox
            // 
            this.infammocheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.infammocheckbox.Checked = false;
            this.infammocheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.infammocheckbox.IsButton = false;
            this.infammocheckbox.Location = new System.Drawing.Point(383, 153);
            this.infammocheckbox.Name = "infammocheckbox";
            this.infammocheckbox.Size = new System.Drawing.Size(30, 15);
            this.infammocheckbox.TabIndex = 30;
            this.infammocheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.infammocheckbox_CheckboxChange);
            // 
            // grenadecheckbox
            // 
            this.grenadecheckbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.grenadecheckbox.Checked = false;
            this.grenadecheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.grenadecheckbox.IsButton = false;
            this.grenadecheckbox.Location = new System.Drawing.Point(392, 137);
            this.grenadecheckbox.Name = "grenadecheckbox";
            this.grenadecheckbox.Size = new System.Drawing.Size(30, 15);
            this.grenadecheckbox.TabIndex = 14;
            this.grenadecheckbox.CheckboxChange += new System.EventHandler<CyberpunkTrainer.controls.CheckboxChangeArgs>(this.grenadecheckbox_CheckboxChange);
            // 
            // roundedObject1
            // 
            this.roundedObject1.Border = 0;
            this.roundedObject1.BorderColor = System.Drawing.Color.Transparent;
            this.roundedObject1.Radius = 8;
            this.roundedObject1.TargetControl = this;
            // 
            // moveAbleWindow1
            // 
            this.moveAbleWindow1.ControlTarget = null;
            this.moveAbleWindow1.MainWindowTarget = this;
            // 
            // roundedObject2
            // 
            this.roundedObject2.Border = 0;
            this.roundedObject2.BorderColor = System.Drawing.Color.Transparent;
            this.roundedObject2.Radius = 8;
            this.roundedObject2.TargetControl = this.healthpanel;
            // 
            // fadeControl1
            // 
            this.fadeControl1.Target = this;
            // 
            // roundedObject3
            // 
            this.roundedObject3.Border = 0;
            this.roundedObject3.BorderColor = System.Drawing.Color.Transparent;
            this.roundedObject3.Radius = 8;
            this.roundedObject3.TargetControl = this.panel5;
            // 
            // roundedObject4
            // 
            this.roundedObject4.Border = 0;
            this.roundedObject4.BorderColor = System.Drawing.Color.Transparent;
            this.roundedObject4.Radius = 8;
            this.roundedObject4.TargetControl = this.panel4;
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CyberpunkTrainer.Properties.Resources.cyberpunk1;
            this.ClientSize = new System.Drawing.Size(600, 498);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.moneycheckbox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.attributeperkcheckbox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.nopolicecheckbox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.manacheckbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.infammocheckbox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.grenadecheckbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.version);
            this.Controls.Add(this.healthpanel);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Window";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cyberpunk2077 Trainer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.onclosecleanup);
            this.Load += new System.EventHandler(this.Window_Load);
            this.healthpanel.ResumeLayout(false);
            this.healthpanel.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private controls.RoundedObject roundedObject1;
        private controls.MoveAbleWindow moveAbleWindow1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel healthpanel;
        private controls.CustomCheckbox instakillcheckbox;
        private controls.CustomCheckbox godmodecheckbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private controls.CustomCheckbox healthfunccheckbox;
        private System.Windows.Forms.Label label1;
        private controls.RoundedObject roundedObject2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label version;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label hookstat;
        private System.Windows.Forms.Label label12;
        private controls.FadeControl fadeControl1;
        private System.ComponentModel.BackgroundWorker worker;
        private System.Windows.Forms.Button button1;
        private controls.CustomCheckbox grenadecheckbox;
        private System.Windows.Forms.Label label4;
        private controls.CustomCheckbox infammocheckbox;
        private System.Windows.Forms.Label label5;
        private controls.CustomCheckbox manacheckbox;
        private System.Windows.Forms.Label label7;
        private controls.CustomCheckbox nopolicecheckbox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolTip toolTip1;
        private controls.CustomCheckbox attributeperkcheckbox;
        private System.Windows.Forms.Label label8;
        private controls.CustomCheckbox moneycheckbox;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.CheckBox mutecheckbox;
        private System.Windows.Forms.Panel panel5;
        private controls.CustomCheckbox backpackdmgcheckbox;
        private System.Windows.Forms.Label label13;
        private controls.CustomCheckbox backpackcheckbox;
        private System.Windows.Forms.Label label16;
        private controls.RoundedObject roundedObject3;
        private System.Windows.Forms.Panel panel6;
        private controls.CustomCheckbox streetcredcheckbox;
        private System.Windows.Forms.Label label18;
        private controls.CustomCheckbox levelxpcheckbox;
        private System.Windows.Forms.Label label11;
        private controls.CustomCheckbox inventoryxpcheckbox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label modulebaseaddr;
        private System.Windows.Forms.Label label20;
        private controls.RoundedObject roundedObject4;
    }
}

