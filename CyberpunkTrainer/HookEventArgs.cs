﻿namespace CyberpunkTrainer
{
    public class HookEventArgs
    {
        private bool status;
        public HookEventArgs(bool status)
        {
            this.status = status;
        }

        public bool Status
        {
            get { return this.status; }
        }

    }
}